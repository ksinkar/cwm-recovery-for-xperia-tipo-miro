#!/bin/bash

function usage
{
    echo Usage:
    echo "  setup.sh tipo for setting tipo"
    echo "  setup.sh miro for setting miro"
    exit 0       
}

echo 
echo 
echo "***********************************************"
echo "          CWM for xperia tipo/miro/j           "
echo "***********************************************"
echo 
echo 

MODEL=$1

FILESDIR=$PWD/bootable/recovery/files/$1

FILES=$FILESDIR/$1

CMDIR=$PWD

DEVICE=""

if [ $MODEL == "tipo" ]; then
	DEVICE="ST21i"

elif [ $MODEL == "miro" ]; then
	DEVICE="ST23i"

elif [ $MODEL == "j" ]; then
	DEVICE="ST26i"

else
	echo "Error:Unknown device $1"
	exit 0
	
fi

echo 
echo 
echo "Building CWM for $1"
echo 
echo 
. build/envsetup.sh
lunch full_$DEVICE-eng
make -j2 recoveryimage
mkdir offline_recovery
mkdir offline_recovery/$1
cp -r $FILESDIR/recovery/* ./offline_recovery/$1/
cd $CMDIR/out/target/product/$DEVICE/recovery/root/
tar -cvf $CMDIR/offline_recovery/$1/files/recovery.tar *
cd ../../../../../../	
echo "Done."
echo
echo "Now go to $PWD/offline_recovery/$1 and use install.sh"
echo "to install the cwm to $1"

