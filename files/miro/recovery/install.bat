@echo off
rem ***********************************************************************
rem ***********************************************************************
rem 
rem                 CWM Recovery for Xperia Tipo/Miro Installer
rem                           -srl3gx
rem ***********************************************************************
rem ***********************************************************************

cd files

adb kill-server
adb start-server

echo =============================================
echo Step1 : Waiting for Device.
echo =============================================
adb wait-for-device
echo OK
echo.

echo =============================================
echo Step2 : Sending some files.
echo =============================================
adb shell "mkdir /data/local/tmp/cwm"
adb push battery_charging /data/local/tmp/cwm
adb push battery_charging_help /data/local/tmp/cwm
adb push recovery.tar /data/local/tmp/cwm
adb push sh /data/local/tmp/cwm
adb push step3.sh /data/local/tmp/cwm

echo.
echo =============================================
echo Step3 : Setting up files system partition.
echo =============================================
adb shell "chmod 755 /data/local/tmp/cwm/sh"
adb shell "chmod 755 /data/local/tmp/cwm/step3.sh"
adb shell "su -c /data/local/tmp/cwm/step3.sh"
adb shell "rm -r /data/local/tmp/cwm"

adb kill-server

echo.
echo Finished!
echo.

pause
